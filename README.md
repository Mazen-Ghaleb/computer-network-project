# Reliable Transport Protocol
The project requires to write and develop the functions A_output(), A_input(), A_timerinterrupt(), A_init(), B_input(), and B_init() in prog2.c for each protocol : Alternating-Bit Protocol  and Go-Back-N version 

The file '18P2491_AB.c' implements the Alternating - Bit Protocol (rdt3.0) as required.

The file '18P2491_GBN.c' implements the Go-Back-N version as required.

The architecture , the project requirements and the project report are also in the repo.

The project report goes in depth on the test cases as well as explains the function implementation and shows the output of the cases.
